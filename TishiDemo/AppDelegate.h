//
//  AppDelegate.h
//  TishiDemo
//
//  Created by 孙哈哈 on 2017/12/9.
//  Copyright © 2017年 SunDe. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

