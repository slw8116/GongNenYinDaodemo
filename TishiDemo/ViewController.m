//
//  ViewController.m
//  TishiDemo
//
//  Created by 孙哈哈 on 2017/12/9.
//  Copyright © 2017年 SunDe. All rights reserved.
//

#import "ViewController.h"

@interface ViewController () <UITableViewDelegate, UITableViewDataSource>

{
    UIView *bgView;
    UIBezierPath *path1;
    CAShapeLayer *maskL;
    CGRect _firstCellR;
    CGRect _secondCellR;
    UIButton *btn1;
    
    NSInteger _guideCount;
}

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    self.view.backgroundColor = [UIColor greenColor];
    
    self.title = @"首页";
    
    UITableView *tableView = [[UITableView alloc] initWithFrame:self.view.bounds];
    tableView.delegate = self;
    tableView.dataSource = self;
    tableView.rowHeight = 60;
    [self.view addSubview:tableView];
    
    UIView *headV = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 0, 150)];
    headV.backgroundColor = [UIColor orangeColor];
    tableView.tableHeaderView = headV;

}

- (void)addGuideView {
    
    _guideCount = 1;
    
    bgView = [[UIView alloc] initWithFrame:self.view.bounds];
    bgView.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.7];
    [self.view addSubview:bgView];
    
    path1 = [UIBezierPath bezierPathWithRect:self.view.bounds];
    UIBezierPath *path2 = [[UIBezierPath bezierPathWithRoundedRect:_firstCellR cornerRadius:30] bezierPathByReversingPath];
//    两个路径拼接
    [path1 appendPath:path2];
    
//    设置mask遮罩
    maskL = [[CAShapeLayer alloc] init];
    maskL.path = path1.CGPath;
    bgView.layer.mask = maskL;
    
    btn1 = [[UIButton alloc] initWithFrame:CGRectMake(100, _firstCellR.origin.y+60, 200, 100)];
    [btn1 setTitle:@"点我查看下一步" forState:0];
    btn1.backgroundColor = [UIColor purpleColor];
    [btn1 addTarget:self action:@selector(btn1Click:) forControlEvents:64];
    [bgView addSubview:btn1];
}

- (void)btn1Click:(UIButton *)sender {
    
    _guideCount++;
    
    if (_guideCount == 2) {
        
//        重新赋值
        path1 = [UIBezierPath bezierPathWithRect:self.view.bounds];
        UIBezierPath *path3 = [[UIBezierPath bezierPathWithRoundedRect:_secondCellR cornerRadius:0] bezierPathByReversingPath];
        [path1 appendPath:path3];
        maskL.path = path1.CGPath;
        bgView.layer.mask = maskL;
        
//        更改按钮信息
        sender.frame = CGRectMake(10, _secondCellR.origin.y+60, 200, 100);
        sender.backgroundColor = [UIColor orangeColor];
        [btn1 setTitle:@"点我引导结束" forState:0];
    }
    
//    最后结束引导，移除view
    if (_guideCount == 3) {
        bgView.hidden = YES;
        [bgView removeFromSuperview];
    }
}

#pragma mark tableView delegate
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return 20;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"CELLID"];
    
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"CELLID"];
    }
    
    if (indexPath.row == 0 && CGRectEqualToRect(_firstCellR, CGRectZero)) {
        _firstCellR = [self cellFrameInScreen:tableView indexPath:indexPath];
        [self addGuideView];
    }
    
    if (indexPath.row == 1  && CGRectEqualToRect(_secondCellR, CGRectZero)) {
        _secondCellR = [self cellFrameInScreen:tableView indexPath:indexPath];
    }
    
    cell.textLabel.text = [NSString stringWithFormat:@"第%ld个细胞", indexPath.row];
    
    return cell;
}

//获得cell在self.view上的位置
- (CGRect)cellFrameInScreen:(UITableView *)tableView indexPath:(NSIndexPath *)indexPath {

//    cell在tableview上的位置
    CGRect cellFrame = [tableView rectForRowAtIndexPath:indexPath];
//    坐标的转换
    CGRect frame = [tableView convertRect:cellFrame toView:self.view];
    return frame;
}

@end
